package com.desafio_tecnico.service;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;

import javax.inject.Inject;

import com.desafio_tecnico.model.Cidade;
import com.desafio_tecnico.model.Estado;
import com.desafio_tecnico.repository.EstadoRepository;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.WebResource;

public class EstadoService implements Serializable {

private static final long serialVersionUID = 1L;

    @Inject
    private EstadoRepository estadoRepository;
    
    @Inject
    private CidadeService cidadeService;
    
    public Estado buscarPorId(Long id) {
    	return estadoRepository.porId(id);
    }
    
    public List<Estado> buscarTodosEstados() {
    	return estadoRepository.todos();
    }
    
    public Estado buscarPorSigla(String sigla) {
    	return estadoRepository.buscarPorSigla(sigla);
    }
    
    public Integer buscarPopulacao(Estado estado) {
    	return estadoRepository.buscarPopulacao(estado);
    }
	
	public BigDecimal calcularCustoPopulacaoEstado(Estado estado, List<Cidade> cidadesParaAdicionar) {
		Integer totalPopulacao = buscarPopulacao(estado);
		BigDecimal valorTotalPopulacao;
		
		for(Cidade cidade : cidadesParaAdicionar) {
			totalPopulacao += cidade.getQuantidadePopulacao().intValue();
		}
		
		valorTotalPopulacao = cidadeService.calculoCustoPopulacaoCidade(totalPopulacao);
		
		valorTotalPopulacao = valorTotalPopulacao.multiply(buscarCotacao());		
		return valorTotalPopulacao.setScale(2, BigDecimal.ROUND_HALF_UP);
	}
	
	public BigDecimal calcularCustoEstado(Estado estado) {
		Integer quantidadePopulacao = buscarPopulacao(estado);
		BigDecimal valorTotalPopulacao;
		
		valorTotalPopulacao = cidadeService.calculoCustoPopulacaoCidade(quantidadePopulacao);
		
		valorTotalPopulacao = valorTotalPopulacao.multiply(buscarCotacao());		
		return valorTotalPopulacao.setScale(2, BigDecimal.ROUND_HALF_UP);
	}
	
	public BigDecimal buscarCotacao() {
		Client c = Client.create();
	    WebResource wr = c.resource("https://economia.awesomeapi.com.br/all/USD-BRL");
	    String json = wr.get(String.class);

	    return extrairCotacaoJson(json);
	}
	
	private BigDecimal extrairCotacaoJson(String json) {
		JsonParser parser = new JsonParser();
		JsonObject obj = parser.parse(json).getAsJsonObject();
		BigDecimal cotacao = new BigDecimal(obj.get("USD").getAsJsonObject().get("ask").getAsString());
		return cotacao;
	}

	
}
