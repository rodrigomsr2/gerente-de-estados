package com.desafio_tecnico.service;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.inject.Inject;

import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.primefaces.model.UploadedFile;

import com.desafio_tecnico.model.Cidade;
import com.desafio_tecnico.repository.EstadoRepository;
import com.desafio_tecnico.util.Constants;

public class ArquivoService {
	
	@Inject
	private EstadoRepository estadoRepository;
	
	public List<Cidade> extrairCidadesEmLote(UploadedFile arquivo) throws IOException {
		String nomeArquivo = arquivo.getFileName();
    	String extensao = nomeArquivo.substring(nomeArquivo.lastIndexOf('.'), nomeArquivo.length());
    	List<Cidade> cidades = new ArrayList<Cidade>();
    	
    	try {
        	InputStream inputStream = arquivo.getInputstream();
        	
        	if (extensao.equals(Constants.EXTENSAO_XLSX)) {
        		XSSFWorkbook workbookXssf = new XSSFWorkbook(inputStream);   
        		XSSFSheet sheetXssf = workbookXssf.getSheetAt(0);
        		cidades = extrairCidades(sheetXssf);
        		workbookXssf.close();
        	} else {
        		HSSFWorkbook workbookHssf = new HSSFWorkbook(inputStream);
        		HSSFSheet sheetHssf = workbookHssf.getSheetAt(0);
        		cidades = extrairCidades(sheetHssf);
        		workbookHssf.close();
        	}
        	
        	inputStream.close();
		} catch (IOException e) {
			throw e;
		}
    	
	    return cidades;
	}
	
	private List<Cidade> extrairCidades(Sheet sheet) {
		List<Cidade> cidades = new ArrayList<Cidade>();

       Iterator<Row> rowIterator = sheet.iterator();

       while (rowIterator.hasNext()) {
              Row row = rowIterator.next();
              Iterator<Cell> cellIterator = row.cellIterator();

              Cidade cidade = new Cidade();
              while (cellIterator.hasNext()) {
                     Cell cell = cellIterator.next();
                     switch (cell.getColumnIndex()) {
                     case 0:
                    	 cidade.setNome(cell.getStringCellValue());
                           break;
                     case 1:
                    	 cidade.setEstado(estadoRepository.buscarPorSigla(cell.getStringCellValue()));
                           break;
                     case 2:
                    	 cidade.setQuantidadePopulacao(new Double(cell.getNumericCellValue()).longValue());
                           break;
                     }
              }
              cidades.add(cidade);
       }
	    
	    return cidades;
	}
	
}
