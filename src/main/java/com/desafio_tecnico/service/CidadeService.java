package com.desafio_tecnico.service;

import java.io.IOException;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;

import javax.inject.Inject;

import org.primefaces.model.UploadedFile;

import com.desafio_tecnico.exception.CidadeParaEstadoJaExistenteException;
import com.desafio_tecnico.exception.ExclusaoCidadeRsException;
import com.desafio_tecnico.model.Cidade;
import com.desafio_tecnico.model.Estado;
import com.desafio_tecnico.repository.CidadeRepository;
import com.desafio_tecnico.repository.EstadoRepository;
import com.desafio_tecnico.util.Constants;
import com.desafio_tecnico.util.Transacional;


public class CidadeService implements Serializable {
    private static final long serialVersionUID = 1L;
    
    private static final String RIO_GRANDE_DO_SUL = "Rio Grande do Sul";

    @Inject
    private CidadeRepository cidadeRepository;
    
    @Inject
    private EstadoRepository estadoRepository;
    
    @Inject
    private ArquivoService arquivoService;

    @Transacional
    public void salvar(Cidade cidade) throws CidadeParaEstadoJaExistenteException {
		cidadeRepository.salvar(cidade);
    }
    
    public Cidade buscarPorId(Long id) {
    	return cidadeRepository.porId(id);
    }
    
    public Integer buscarTotalPopulacao(Estado estado) {
    	return estadoRepository.buscarPopulacao(estado);
    }
    
    public List<Cidade> buscarPorEstado(Estado estado) {
    	return cidadeRepository.buscarPorEstado(estado);
    }

    @Transacional
    public void excluir(Cidade cidade) throws ExclusaoCidadeRsException {
    	
    	if (RIO_GRANDE_DO_SUL.equals(cidade.getEstado().getNome())) {
    		throw new ExclusaoCidadeRsException("Não é possível excluir uma cidade do estado do Rio Grande do Sul");
    	}
    	
    	cidadeRepository.remover(cidade);
    }
    
    @Transacional
    public void inserirArquivosEmLote(UploadedFile file) throws CidadeParaEstadoJaExistenteException, IOException {
    	List<Cidade> cidades = arquivoService.extrairCidadesEmLote(file);
    	
    	for(Cidade cidade : cidades) {
			this.cidadeRepository.salvar(cidade);
    	}
    }
    
	public BigDecimal calculoCustoPopulacaoCidade(Integer quantidadePopulacao) {
		Integer quantidadeCidadoesComDesconto = 0;
		BigDecimal valorComDesconto = new BigDecimal(0);
		BigDecimal valorSemDesconto = new BigDecimal(0);
		
		if(Constants.VALOR_CORTE.compareTo(quantidadePopulacao) < 0) {
			quantidadeCidadoesComDesconto = quantidadePopulacao - Constants.VALOR_CORTE;
			valorComDesconto = getCustoCidadaoDesconto().multiply(new BigDecimal(quantidadeCidadoesComDesconto));
		}
		
		if(valorComDesconto.compareTo(new BigDecimal(0)) <= 0) {
			valorSemDesconto = Constants.CUSTO_CIDADAO.multiply(new BigDecimal(quantidadePopulacao));
		} else {			
			valorSemDesconto = Constants.CUSTO_CIDADAO.multiply(
								new BigDecimal(quantidadePopulacao - quantidadeCidadoesComDesconto));
		}
				
		return valorComDesconto.add(valorSemDesconto);
	}
	
	private BigDecimal getCustoCidadaoDesconto() {
		BigDecimal desconto = Constants.CUSTO_CIDADAO.multiply(Constants.PORCENTAGEM_DESCONTO).divide(new BigDecimal(100));
		return Constants.CUSTO_CIDADAO.subtract(desconto);
	}
	
}
