package com.desafio_tecnico.converter;

import java.util.List;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;

import com.desafio_tecnico.model.Estado;

public class EstadoConverter implements Converter {
	
	private List<Estado> estados;
	
	public EstadoConverter() {}
	
	public EstadoConverter(List<Estado> estados) {
		this.estados = estados;
	}
	
	@Override
	public Object getAsObject(FacesContext context, UIComponent component, String value) {
		if (value == null) {
		    return null;
		}
		
		return estados.stream().filter(estado -> Long.valueOf(value).equals(estado.getId())).findAny().orElse(null);
	}

	@Override
	public String getAsString(FacesContext context, UIComponent component, Object value) {
		if (value == null) {
		    return null;
		}
		
		Estado estado = (Estado) value;
		
		return estado.getId().toString();
	}

}
