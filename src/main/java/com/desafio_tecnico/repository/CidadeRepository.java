package com.desafio_tecnico.repository;

import java.io.Serializable;
import java.util.List;

import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;

import com.desafio_tecnico.exception.CidadeParaEstadoJaExistenteException;
import com.desafio_tecnico.model.Cidade;
import com.desafio_tecnico.model.Estado;

public class CidadeRepository implements Serializable {

	private static final long serialVersionUID = 1L;

	@Inject
	private EntityManager manager;

	public CidadeRepository() {

	}

	public CidadeRepository(EntityManager manager) {
		this.manager = manager;
	}

	public Cidade porId(Long id) {
		return manager.find(Cidade.class, id);
	}

	public List<Cidade> pesquisar(String nome) {
		String jpql = "from Cidade where nome like :nome";
		
		TypedQuery<Cidade> query = manager
				.createQuery(jpql, Cidade.class);
		
		query.setParameter("nome", "%" + nome + "%");
		
		return query.getResultList();
	}
	
	public List<Cidade> buscarPorEstado(Estado estado) {
		String jpql = "from Cidade c where c.estado = :estado";
		
		TypedQuery<Cidade> query = manager
				.createQuery(jpql, Cidade.class);
		
		query.setParameter("estado", estado);
		
		return query.getResultList();
	}

	public Cidade salvar(Cidade cidade) throws CidadeParaEstadoJaExistenteException {
		try {
			return manager.merge(cidade);
    	} catch (Exception e) {
    		throw new CidadeParaEstadoJaExistenteException("Esta cidade já existe neste estado!");
    	}
	}

	public void remover(Cidade cidade) {
		cidade = porId(cidade.getId());
		manager.remove(cidade);
	}
	
}
