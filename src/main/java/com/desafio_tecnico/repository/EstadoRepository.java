package com.desafio_tecnico.repository;

import java.io.Serializable;
import java.util.List;

import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;

import com.desafio_tecnico.model.Estado;

public class EstadoRepository implements Serializable {

	private static final long serialVersionUID = 1L;

	@Inject
	private EntityManager manager;

	public EstadoRepository() {

	}

	public EstadoRepository(EntityManager manager) {
		this.manager = manager;
	}

	public Estado porId(Long id) {
		return manager.find(Estado.class, id);
	}
	
	public Integer buscarPopulacao(Estado estado) {
		
		TypedQuery<Long> query = manager.createQuery("select sum(c.quantidadePopulacao) "
														+ "from Cidade c "
														+ "join c.estado e "
														+ "where e.id = :id "
														+ "group by e.nome ",
														Long.class);
		
		query.setParameter("id", estado.getId());
		return query.getSingleResult().intValue();
	}
	
	public Estado buscarPorSigla(String sigla) {
		String jpql = "from Estado e where e.sigla = :sigla";
		
		TypedQuery<Estado> query = manager
				.createQuery(jpql, Estado.class);
		
		query.setParameter("sigla", sigla);
		
		return query.getSingleResult();
	}

	
	public List<Estado> todos() {
		return manager.createQuery("from Estado", Estado.class).getResultList();
	}
	
}
