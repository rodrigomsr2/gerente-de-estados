package com.desafio_tecnico.util;

import java.math.BigDecimal;

public class Constants {

	public static final BigDecimal CUSTO_CIDADAO = new BigDecimal(123.45);
	
	public static final BigDecimal PORCENTAGEM_DESCONTO = new BigDecimal(12.30);
	
	public static final Integer VALOR_CORTE = 50000;
	
	public static final String PAGINA_LISTAGEM_CIDADES = "cidades.xhtml?i=0&faces-redirect=true";
	
	public static final String PAGINA_LISTAGEM_ESTADOS = "estados.xhtml?i=1&faces-redirect=true";
	
	public static final String PAGINA_NOVA_CIDADE = "novaCidade.xhtml?i=2&faces-redirect=true";
	
	public static final String PAGINA_IMPORTAR_CIDADES_EM_LOTE = "importarCidadesEmLote.xhtml?i=2&faces-redirect=true";
	
	public static final String EXTENSAO_XLSX = ".xlsx";
	
}
