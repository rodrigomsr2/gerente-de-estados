package com.desafio_tecnico.controller;

import java.io.IOException;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;

import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

import com.desafio_tecnico.converter.EstadoConverter;
import com.desafio_tecnico.model.Estado;
import com.desafio_tecnico.service.EstadoService;
import com.desafio_tecnico.util.Constants;

@Named
@ViewScoped
public class EstadoController implements Serializable  {
	
	private static final long serialVersionUID = 1L;
	
	@Inject
	private EstadoService estadoService;
	
	private Estado estadoSelecionado;
	
	private List<Estado> estados;
	
	private Converter estadoConverter;
	
	public void init() {
		this.estados = this.estadoService.buscarTodosEstados();
		estadoConverter = new EstadoConverter(estados);
		setUpEstados();
	}
	
	public void setUpEstados() {
		for(Estado estado : estados) {
			estado.setPopulacao(estadoService.buscarPopulacao(estado));
			estado.setValorPopulacao(estadoService.calcularCustoEstado(estado));
		}
	}
	
	public void redirectToListagemCidades() throws IOException {
		FacesContext.getCurrentInstance().getExternalContext().redirect(Constants.PAGINA_LISTAGEM_CIDADES);
	}

	public Estado getEstadoSelecionado() {
		return estadoSelecionado;
	}

	public void setEstadoSelecionado(Estado estadoSelecionado) {
		this.estadoSelecionado = estadoSelecionado;
	}
	
	public BigDecimal getCustoEstado() {
		if (this.estadoSelecionado.getId() == null) {
			return null;
		}
		return this.estadoService.calcularCustoEstado(this.estadoSelecionado);
	}
	
	public void selecionarEstado(Estado estado) {
		this.estadoSelecionado = estado;
	}
	
	public Converter getEstadoConverter() {
		return estadoConverter;
	}

	public List<Estado> getEstados() {
		return estados;
	}

	public void setEstados(List<Estado> estados) {
		this.estados = estados;
	}
	
}

