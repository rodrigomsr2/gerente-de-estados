package com.desafio_tecnico.controller;

import java.io.IOException;
import java.io.Serializable;
import java.util.List;

import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.event.AjaxBehaviorEvent;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

import org.apache.commons.lang3.StringUtils;
import org.primefaces.model.UploadedFile;

import com.desafio_tecnico.converter.EstadoConverter;
import com.desafio_tecnico.exception.CidadeParaEstadoJaExistenteException;
import com.desafio_tecnico.exception.ExclusaoCidadeRsException;
import com.desafio_tecnico.model.Cidade;
import com.desafio_tecnico.model.Estado;
import com.desafio_tecnico.service.CidadeService;
import com.desafio_tecnico.service.EstadoService;
import com.desafio_tecnico.util.Constants;
import com.desafio_tecnico.util.FacesMessages;

@Named
@ViewScoped
public class CidadeController implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	@Inject
	private CidadeService cidadeService;
	
	@Inject
	private EstadoService estadoService;
	
    @Inject
    private FacesMessages messages;
	
	private Cidade cidadeSelecionada;
	
	private List<Estado> estados;
	
	private List<Cidade> cidades;
	
	private Estado estadoSelecionado;
	
	private Converter estadoConverter;
	
	private Cidade novaCidade;
	
	private UploadedFile file;
	
	public void init() throws CidadeParaEstadoJaExistenteException {
		this.estadoSelecionado = this.estadoService.buscarPorSigla("SC");
		this.estados = this.estadoService.buscarTodosEstados();
		this.cidades = this.cidadeService.buscarPorEstado(estadoSelecionado);
		estadoConverter = new EstadoConverter(estados);
	}
	
	public void initNovaCidade() {
		novaCidade = new Cidade();
		novaCidade.setEstado(this.estadoService.buscarPorSigla("SC"));
		this.estados = this.estadoService.buscarTodosEstados();
		estadoConverter = new EstadoConverter(estados);
	}
	
    public void uploadCidadesEmLote() throws IOException {
    	if (file == null) {
    		messages.info("Você precisa selecionar um arquivo primeiro!");	
    	} else {
    		try {
        		this.cidadeService.inserirArquivosEmLote(file);
        		messages.info("Cidades inseridas com sucesso!");
    		} catch (CidadeParaEstadoJaExistenteException e) {
    			messages.info("O arquivos possui 1 ou mais cidades já cadastradas!");
    		}
    	}
    }
	
	public void salvar() throws IOException {
		try {
			this.cidadeService.salvar(novaCidade);
			FacesContext.getCurrentInstance().getExternalContext().redirect(Constants.PAGINA_LISTAGEM_CIDADES);
		} catch (CidadeParaEstadoJaExistenteException e) {
			messages.info(e.getMessage());
		}
	}
	
	public void excluir() {
		try {
			this.cidadeService.excluir(cidadeSelecionada);
		} catch(ExclusaoCidadeRsException e) {
			messages.info(e.getMessage());
		}
		
		this.cidadeSelecionada = null;
		this.cidades = this.cidadeService.buscarPorEstado(estadoSelecionado);
	}
	
	public void atualizarCidadePorEstadoSelecionado(final AjaxBehaviorEvent event) {
		this.cidades = this.cidadeService.buscarPorEstado(estadoSelecionado);
	}
	
	public void redirectToListagemEstados() throws IOException  {
		FacesContext.getCurrentInstance().getExternalContext().redirect(Constants.PAGINA_LISTAGEM_ESTADOS);
	}
	
	public void redirectToInsercaoCidade() throws IOException {
		FacesContext.getCurrentInstance().getExternalContext().redirect(Constants.PAGINA_NOVA_CIDADE);
	}
	
	public void redirectToImportarCidadesEmLote() throws IOException {
		FacesContext.getCurrentInstance().getExternalContext().redirect(Constants.PAGINA_IMPORTAR_CIDADES_EM_LOTE);
	}
	
    public boolean isCidadeSelecionadaDisponivel() {
        return cidadeSelecionada != null && cidadeSelecionada.getId() != null;
    }
	
	public Cidade getCidadeSelecionada() {
		return cidadeSelecionada;
	}

	public void setCidadeSelecionada(Cidade cidadeSelecionada) {
		this.cidadeSelecionada = cidadeSelecionada;
	}

	public List<Estado> getEstados() {
		return estados;
	}

	public void setEstados(List<Estado> estados) {
		this.estados = estados;
	}

	public Estado getEstadoSelecionado() {
		return estadoSelecionado;
	}

	public void setEstadoSelecionado(Estado estadoSelecionado) {
		this.estadoSelecionado = estadoSelecionado;
	}

	public List<Cidade> getCidades() {
		return cidades;
	}

	public void setCidades(List<Cidade> cidades) {
		this.cidades = cidades;
	}

	public Cidade getNovaCidade() {
		return novaCidade;
	}

	public void setNovaCidade(Cidade novaCidade) {
		this.novaCidade = novaCidade;
	}

	public Converter getEstadoConverter() {
		return estadoConverter;
	}
	
    public UploadedFile getFile() {
        return file;
    }
 
    public void setFile(UploadedFile file) {
        this.file = file;
    }

}
