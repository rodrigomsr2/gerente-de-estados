package com.desafio_tecnico.exception;

public class ExclusaoCidadeRsException extends Exception {

	private static final long serialVersionUID = 1L;
	
	public ExclusaoCidadeRsException(String message) {
		super(message);
	}

}
