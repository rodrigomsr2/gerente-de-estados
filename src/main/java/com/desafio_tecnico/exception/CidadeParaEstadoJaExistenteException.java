package com.desafio_tecnico.exception;

public class CidadeParaEstadoJaExistenteException extends Exception  {

	private static final long serialVersionUID = 1L;

	public CidadeParaEstadoJaExistenteException(String message) {
		super(message);
	}
	
}
