insert into estado (id, nome, sigla) values (11, 'Rio Grande do Sul', 'RS');
insert into estado (id, nome, sigla) values (12, 'Santa Catarina', 'SC');
insert into estado (id, nome, sigla) values (13, 'Paraná', 'PR');

insert into cidade (id, nome, quantidade_populacao, estado_id) values (1, 'Porto Alegre', '3990', 11);
insert into cidade (id, nome, quantidade_populacao, estado_id) values (2, 'Alegrete', '1000', 11);
insert into cidade (id, nome, quantidade_populacao, estado_id) values (3, 'Florianópolis', '3000', 12);
insert into cidade (id, nome, quantidade_populacao, estado_id) values (4, 'Balneário Camburiu', '500', 12);
insert into cidade (id, nome, quantidade_populacao, estado_id) values (5, 'Curitiba', '1', 13);
insert into cidade (id, nome, quantidade_populacao, estado_id) values (6, 'Cascavel', '1', 13);
