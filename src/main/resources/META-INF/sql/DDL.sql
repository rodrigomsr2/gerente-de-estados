create table cidade (
	id bigint not null auto_increment,
	nome varchar(255) not null,
	quantidade_populacao bigint not null,
	estado_id bigint not null,
	primary key (id)
)

create table estado (
	id bigint not null auto_increment,
	custo_populacional decimal(19,2) not null,
	nome varchar(255),
	primary key (id)
)

alter table cidade 
add constraint UKgx7khulpkfsgyqbw5noivqui7 unique (estado_id, nome)


alter table cidade 
add constraint FKkworrwk40xj58kevvh3evi500 foreign key (estado_id) references estado (id)
	