# README #

### Softwares utilizados para o desenvolvimento ###

* Sistema Operacional: Windows 10 Pro
* IDE: Eclipse IDE for Enterprise Java Developers 4.17.0
* Linguagem de programação: Java 1.8
* Banco de dados: MySQL 8.0.22
	* Usuário: root
	* Senha: root
	* Nome do banco (schema): gerenciador_de_estados
* WebServer: Tomcat 8.5

### Instruções para execução ###

* Criar um novo servidor na aba "Servers" do eclipse
* Em "Overview", selecionar a opção "Use Tomcat installation"
* Na aba "Project Explorer", clicar com o botão direito no projeto -> Run As -> Run on Server -> Selecionar o TomCat 8.5 (previamente cadastrado) -> Finish
* Ao executar a aplicação, as tabelas são geradas automaticamente pelo Hibernate, juntamente com algumas inserções
	* Para que a geração das tabelas seja possível, é necessário ter um banco (schema) com o nome "gerenciador_de_estados" ou alterar esta informção no arquivo "persistence.xml"
	* Os arquivos DDL.sql e DML.sql foram inseridos no projeto com as informações requeridas na atividade, porém não são necessários

### Acessando funcionalidades da aplicação ###

* A aplicação pode ser acessada no browser via URL "http://localhost:8080/GerenciadorDeEstados/"
* A tela inicial é a tela que exibe a funcionalidade de listagem de cidades
* Inserção: Para inserir uma cidade, basta clicar no botão "Adicionar cidade" presente na página de listagem de cidades, preencher os campos e clicar no botão "Salvar"
* Exclusão: Para excluir uma cidade, basta clicar no botão "Excluir" (fica disponível somente após a seleção de uma cidade na tabela) presente na página de listagem de cidades
* Listagem de cidades: Para listar as cidades, basta carregar a página inicial ou acessar a aba "Listagem de cidades", no menu do site
* Listagem de estados: Para listar os estados, basta carregar a página inicial ou acessar a aba "Listagem de estados", no menu do site
* Consultar população dos estados: Informação presente na Listagem de estados
* Consultar custo populacional: Informação presente na Listagem de estados
* Inserção de cidades em lote: Clicar no botão, com símbolo de excel, presente na tela inicial, clicar no botão "Selecionar documento", carregar o documento e clicar no botão "Importar"
	* A inserção de cidades em lote foi testado com documentos de extensão: (i) xlsx e (ii) xls
	* Existe um arquivo chamado "exemplo.xlsx", no projeto, para demonstração do formato do arquivo utilizado para realizar a inserção de cidades em lote
